# Exercicio 015 - Python - Curso em Video
# Aluguel do carro custando 60,00 por dia e 0,15 por km rodado

print()
dias = int(input("Dias Alugados: "))
km = float(input("Km rodados: "))
pago = (dias*60) + (km* 0.15)

print("O total a pagar é de R$ {:.2f}".format(pago))
