# Exercicio 032 - Python - Curso em Video
# Ano bissexto
from datetime import date

print()
ano = int(input("Que ano voce quer analisar? Digite 0 para analisar o ano atual: "))

if ano ==0:
    ano = date.today().year
if ano % 4 == 0 or ano % 100 != 0 or ano % 400 == 0:
    print("O ano {} é BISSEXTO".format(ano))
else:
    print("O ano {} não é BISSEXTO".format(ano))