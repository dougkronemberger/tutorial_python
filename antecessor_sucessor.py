# Exercicio 005 - Python - Curso em Video
# Leia um numero e mostre o antecessor e sucessor

print()
num = int(input("Digite um numero: "))

print("O numero digitado foi {}. Seu Antecessor é {} e seu Sucessor é {}".format(num, (num-1), (num+1)))