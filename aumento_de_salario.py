# Exercicio 013 - Python - Curso em Video
# Reajuste Salarial com 15% de aumento

print()
salario = float(input("Qual o seu salário? R$"))
novo_salario = salario + (salario * 15/100)

print("O seu salário era R$ {:.2f} e com 15% de aumento ficou R$ {:.2f}".format(salario,novo_salario))