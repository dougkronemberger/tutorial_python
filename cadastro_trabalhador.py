# EXERCICIO 92 EM PYTHON - CURSO EM VIDEO
# CADASTRANDO TRABALHADOR
from datetime import datetime

print()
dados = dict()
dados['nome'] = str(input("Nome: "))
nasc = int(input("Data de Nascimento: "))
dados['idade'] = datetime.now().year - nasc
dados['ctps'] = int(input("Carteira de trabalho: (0 não tem) "))

if dados['ctps'] != 0:
    dados['contratacao'] = int(input("Ano de contratação: "))
    dados['salario'] = float(input("Salário: R$ "))
    dados['aposentadoria'] = dados['idade'] + ((dados['contratacao'] + 35) - datetime.now().year)

print("-" * 35)
print(dados)

print("-" * 35)
for k, v in dados.items():
    print(f' - {k}: {v} ')