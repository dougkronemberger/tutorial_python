tabela = {
    "1" : 1.5,
    "2" : 3,
    "3" : 7.4,
    "5" : 0.75
}

compras = []

total_compra = 0.0

registrando = True

while(registrando):
    codigo = str(input('informe o código do produto, ou zero para sair: '))
    if codigo == "0":
        registrando = False
    else:
        if(codigo in tabela):
            quantidade = float(input('forneça a quantidade: '))
            compras.append({
                "codigo"     : codigo,
                "quantidade" : quantidade
            })
        else:
            print('produto não existe, retornando\n\n')
            
print('\n\nfim da compra\n-----------------------------------------')

for i, compra in enumerate(compras):
    
    codigo = compra['codigo']
    quantidade = compra['quantidade']
    preco = tabela[codigo]
    
    subtotal = preco * quantidade
    total_compra = total_compra + subtotal
    
    print('codigo:', codigo, '  valor unitário:', preco, '  quantidade:', quantidade, '  subtotal:', subtotal)
    
print('-----------------------------------------\ntotal da compra:', total_compra)