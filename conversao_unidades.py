# Exercicio 008 - Python - Curso em Video

medida = float(input("Digite uma medida em metros: "))
cm = medida*100
mm = medida*1000

print("Medida em cm: {:.0f} cm".format(cm))
print("Medida em mm: {:.0f} mm".format(mm))