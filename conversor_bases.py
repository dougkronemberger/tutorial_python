#Conversor de Bases Numéricas Python (Desafio 37 Curso Em Video)
num = int(input("Digite o numero que voce quer converter: "))

print('''
Qual base voce deseja converter:
[1] BINARIO
[2] OCTAL
[3] HEXADECIMAL
''')

opcao = int(input("Sua opcao: "))

if opcao == 1:
    print("{} convertido em BINARIO é: {}".format(num,bin(num)[2:])) # 0b
elif opcao == 2:
    print("{} convertido em OCTAL é: {}".format(num,oct(num)[2:])) # 0o
elif opcao == 3:
    print("{} convertido em HEXADECIMAL é: {}".format(num,hex(num)[2:])) # 0x
else:
    print("Opção invalida...")