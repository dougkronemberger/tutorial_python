# Sistema de Quiz usando dicionários:

score = 0
respostas_certas = 0

perguntas = {
    'Pergunta 1': {
        'pergunta': 'Quanto é 2+2? ',
        'respostas': {'a': '1','b': '4', 'c': '2'},
        'resposta_certa': 'b',
    },
    'Pergunta 2': {
        'pergunta': 'Quanto é 10-10*10+10? ',
        'respostas': {'a': '-80','b': '100', 'c': '0'},
        'resposta_certa': 'a',
    },
}


print()

for pk, pv in perguntas.items(): # pk = pergunta_key / pv = pergunta_velha
    print(f'{pk}: {pv["pergunta"]}')
    
    print()
    
    for rk, rv in pv['respostas'].items(): # rk = resposta_key / rv = resposta_velha
        print(f'[{rk}]: {rv}')
        
    resposta_usuario = input("Qual a resposta: ")
    
    if resposta_usuario == pv['resposta_certa']:
        print("Voce acertou...")
        respostas_certas += 1
    else:
        print("Voce errou...")
    
    print()
    
qtd_perguntas = len(perguntas)
porcentagem_acerto = respostas_certas/qtd_perguntas * 100

print(f'Quantidade de acertos: {respostas_certas} ')
print(f'Porcentagem de acertos: {porcentagem_acerto}%')