# Exercicio 006 - Python - Curso em Video
# Crie um algoritmo que leia um numero e mostre o dobro, triplo e a raiz quadrada

print()
num = int(input("Digite um numero: "))

print("O numero digitado foi {}.".format(num))
print("O dobro do numero {} é {}.".format(num, (num*2)))
print("O triplo do numero {} é {}.".format(num, (num*3)))
print("A raiz quadrada do numero {} é {:.2f}.".format(num, (num**(1/2))))