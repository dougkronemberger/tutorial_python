# Exercicio 010 - Python - Curso em Video
# Quanto dinheiro voce tem na carteira e quantos dolares voce pode ter?

print()
dolarHoje = float(input("Dolar Hoje: "))
real = float(input("Quanto dinheiro você tem na carteira? "))
dolar = real/dolarHoje

if real < dolarHoje:
    print("Seu saldo em reais é de R$ {:.2f}. Dólar Hoje: U$$ {:.2f}. Não é possível comprar.".format(real,dolarHoje))
else:
    print("Voce tem na carteira R$ {:.2f} reais e você pode comprar U$${:.0f} dólar(es).".format(real,dolar))