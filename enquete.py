# Declara um vetor com 23 posicoes inicializando ele com o valor zero
sistemasOperacionais = (
    'Windows Server', 'Unix', 'Linux', 'Netware', 'Mac OS', 'Outro')
votosSistemas = [0] * 6
opcao = -1
totalVotos = 0
print('Qual o melhor Sistema Operacional para uso em servidores?')
print('\n As possiveis respostas sao: \n')

i = 0
for sistema in sistemasOperacionais:
    print('%d- %s' % (i + 1, sistemasOperacionais[i]))
    i += 1
    
print()

while (opcao != 0):
    opcao = int(input('Sistema escolhido (0=fim): '))
    if (opcao < 0) or (opcao > 23):
        print('Informe um valor entre 1 e 6 ou 0 para sair!')
        continue
    if (opcao != 0):
        votosSistemas[opcao - 1] += 1
        totalVotos += 1

print()
print("--" * 21)

i = 0
maiorVoto = 0

for sistema in sistemasOperacionais:
    # Formatacao de tabela
    print('%-21s %5d   %2.2f' %(
       (sistemasOperacionais[i], votosSistemas[i],
       votosSistemas[i] / float(totalVotos) * 100)))
    if (votosSistemas[i] > votosSistemas[maiorVoto]):
        maiorVoto = i
    i += 1

print("--" * 21)
# Formatacao de tabela
print('Total: %2d' % totalVotos)

print("--" * 21)
print("Maior Voto: ",sistemasOperacionais[maiorVoto])
print("Qtd de Votos: ",votosSistemas[maiorVoto])
print("% de Votos: ",votosSistemas[maiorVoto] / float(totalVotos) * 100)
print()