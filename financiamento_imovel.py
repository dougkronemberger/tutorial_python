# Exercicio 036 - Python - Curso em Video

print()
imovel = float(input("Valor da Casa/Apartamento? R$ "))
salario = float(input("Salário do Funcionário: R$ "))
anos = int(input("Anos de Financiamento: "))
prestacao = imovel/(anos*12)

print("-" * 50)
print("Para pagar um imóvel de R$ {:.2f} em {} anos".format(imovel, anos))
print("Voce terá uma prestação de R$ {:.2f}".format(prestacao))
