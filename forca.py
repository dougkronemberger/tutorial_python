# Jogo da Forca em Python
import os

palavra_secreta = str(input("Digite a palavra secreta: "))
os.system("cls")
letras_descobertas = []
print()
#print("A Palavra secreta foi: {}".format(palavra_secreta))
print("A palavra secreta tem: {} letras".format(len(palavra_secreta)))

print("Jogo da Forca")

print()
for i in range(0,len(palavra_secreta)):
    letras_descobertas.append("_")
    
acertou = False

while acertou == False:
    print()
    letra = str(input("Digite a letra: "))
    
    for i in range(0,len(palavra_secreta)):
        if letra == palavra_secreta[i]:
            letras_descobertas[i] = letra
        
        print(letras_descobertas[i])
        
    acertou = True
    
    print()
    for x in range(0,len(letras_descobertas)):
        if letras_descobertas[x] == "_":
            acertou = False

print()
print("Parabéns")