# Exercicio 073 - Python - Curso em Video
# Tuplas com Times e Rodadas sem pontuação
from random import shuffle

print()
times = [
"Corinthians","Palmeiras",
"Santos","Gremio",
"Cruzeiro","Flamengo",
"Vasco","Chapecoense",
"Atletico-MG","Athetico-PR",
"Bahia","São Paulo",
"Fluminense","Sport",
"Vitoria","Coritiba",
"Avai","Ponte Preta",
"Botafogo","Atletico-GO"
]
print("-" * 30)
print(f'Lista de Times: {times}')
print("-" * 30)
shuffle(times)
print("-" * 30)
print(f'Os cinco primeiros são: {times[0:5]}')
print("-" * 30)
print(f'Os quatro últimos são: {times[-4:]}')

print("-" * 30)
print("Rodadas")
print("-" * 30)
rod = 0
while(rod <= 19):
    shuffle(times)
    print(f'{times[0]} x {times[1]}')
    rod +=1