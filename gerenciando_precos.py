# Exercicio 044 - Python - Curso em Video

print()
print("-" * 20 + " LOJAS " + "-" * 20)
print()
preco = float(input("Preço das compras: R$ "))

print('''
[1] à vista dinheiro/cheque
[2] à vista cartão
[3] 2x cartão
[4] 3x ou mais no cartão
''')

opcao = int(input("Qual sua opcao? "))
if opcao == 1:
    total = preco - (preco * 10)/100
    print("Preço vai ser R$ {}".format(total))
elif opcao == 2:
    total = preco - (preco * 5)/100
    print("Preço vai ser R$ {}".format(total))
elif opcao == 3:
    parcela = 2
    total = preco/parcela
    print("Sua compra vai ser parcelada em {}x de R$ {:.2f}".format(parcela, total))
elif opcao == 4:
    qtd_parcelas = int(input("Quantas parcelas: "))
    total = preco/qtd_parcelas
    print("Sua compra vai ser parcelada em {}x de R$ {:.2f}".format(qtd_parcelas, total))
else:
    if (opcao == 0) or (opcao != 4):
        print("Opção inválida - Tente de novo.")
        
print()
