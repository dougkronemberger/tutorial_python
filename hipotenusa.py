# Exercicio 017 - Python - Curso em Video
import math

print()
co = float(input("Cateto Oposto: "))
ca = float(input("Cateto Adjacente: "))

hip = math.hypot(co, ca)
print()
print("-" *20)
print("Cateto oposto: {}".format(co))
print("Cateto Adjacente: {}".format(ca))
print("Hipotenusa: {:.2f}".format(hip))