# Programando perguntas e respostas (parecido IA em Python SQN kkk)
from random import randrange

questions = {
	'a': 'Que dia da semana é hoje?',
	'b': 'Como está o clima?'
}

default = {'default': 'Adeus'}

answer1 = ['Domingo','Segunda-feira','Terça-feira','Quarta-feira','Quinta-feira','Sexta-feira','Sábado']
answer2 = ['Ensolarado e quente','Chuvoso e úmido','Frio e seco']

def main():
    while True:
        qst = input("User: ")
        if qst == questions['a']:
            print('Bot: Hoje é %s \n' %answer1[randrange(len(answer1))])
        elif qst == questions['b']:
            print('Bot: Hoje está %s \n' %answer2[randrange(len(answer2))])
        elif qst == default['default']:
            print("Bot: Até mais...")
            break
        else:
            print("Bot: Desculpe, não entendi. \n ")

main()