# Importando seletor aleatório em determinado intervalo
from random import randint

# Guarda numero aleatório
numeroMagico = randint(1, 22)

# Número de tentativas restantes
numeroTentativas = 10

print('NÃO É PERMITIDO:\n- DIGITOS NÃO NÚMERICOS;\n- NÚMEROS OU ESPAÇOS.')
print('\nHumm... Eu pensei em um número de 1 a 22, você tem que adivinhar\n' +
      'ele em menos de 7 rodadas, está preparado?')

# Inicia o Jogo
while True:

    # Verifica se a quantidade de rodadas restantes acabou
    if (numeroTentativas == 0):
        print('Você perdeu!')
        break

    # Verifica se a tentativa inserida é válida
    while True:
        tentativa = input('\nDigite o número que você acha que é: ')

        if not tentativa.isdigit():
            print('NÃO É PERMITIDO:\n- DIGITOS NÃO NÚMERICOS;\n- NÚMEROS OU ESPAÇOS.\n')
        else:
            tentativa = int(tentativa)
            break
    
    # Jogar Vence
    if (tentativa == numeroMagico):
        print('Parabéns, você Venceu!!!')
        break
    
    # Número sugerido é muito baixo e diminui número de tentativas
    elif (tentativa < numeroMagico):
        print('Seu número é muito baixo.')
        numeroTentativas -= 1
        print("Voce tem: ", numeroTentativas, " tentativas")
    
    # Número sugerido é muito alto e diminui número de tentativas
    else:
        print('Seu número é muito alto.')
        numeroTentativas -= 1
        print("Voce tem: ", numeroTentativas, " tentativas")
        
print("-" * 15)
print("Eu pensei no numero: ", numeroMagico)
print("Voce digitou o numero: ", tentativa)