# Exercicio 028 - Python - Curso em Video
# Jogo da Adivinhação
from random import randint

print()
computador = randint(0,5) # Faz o computador pensar
print("-" * 20)
print("Vou pensar em um número: ")
print("-" * 20)
jogador = int(input("Em que numero eu pensei? ")) # Jogador tenta adivinhar

if jogador == computador:
    print("Voce venceu...")
else:
    print("Eu pensei no numero {} e não no {}".format(computador, jogador))
    print("Voce perdeu...")