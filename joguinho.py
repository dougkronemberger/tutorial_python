# DIGITAR OS NUMEROS E VERIFICAR ONDE ELES ESTÃO ATÉ QUE UM DIGITO SEJA NEGATIVO

numero = 0
intervalo = [0, 0, 0, 0]
    
while (numero >= 0):
    numero = int(input("Digite um número: "))
    if (numero >= 0):
        if (numero <= 25):
            intervalo[0] = intervalo[0] + 1
        elif (numero <= 51):
            intervalo[1] = intervalo[1] + 1
        elif (numero <= 75):
            intervalo[2] = intervalo[2] + 1
        elif (numero <= 100):
            intervalo[3] = intervalo[3] + 1
                
print("Números no intervalo [0-25]:", intervalo[0])
print("Números no intervalo [26-50]:", intervalo[1])
print("Números no intervalo [51-75]:", intervalo[2])
print("Números no intervalo [76-100]:", intervalo[3])