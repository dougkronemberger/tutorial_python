# Exercicio 045 - Python - Curso em Video
# Pedra Papel e Tesoura
from random import randint

print('''
Suas Opções:
[0] Pedra
[1] Papel
[2] Tesoura
''')

print()
itens = ("Pedra","Papel","Tesoura")
computador = randint(0,2)
jogador = int(input("Qual é a sua jogada? "))
print("-" * 40)
print("O computador escolheu: {}".format(itens[computador]))
print("O jogador escolheu: {}".format(itens[jogador]))
print("-" * 40)

# Computador jogou PEDRA
if computador == 0:
    if jogador == 0:
        print("EMPATE")
    elif jogador == 1:
        print("JOGADOR VENCEU")
    elif jogador == 2:
        print("COMPUTADOR VENCEU")
    else:
        print("JOGADA INVÁLIDA")
        
# Computador jogou PAPEL
if computador == 1:
    if jogador == 0:
        print("COMPUTADOR VENCEU")
    elif jogador == 1:
        print("EMPATE")
    elif jogador == 2:
        print("JOGADOR VENCEU")
    else:
        print("JOGADA INVÁLIDA")
        
# Computador jogou TESOURA
if computador == 2:
    if jogador == 0:
        print("JOGADOR VENCEU")
    elif jogador == 1:
        print("COMPUTADOR VENCEU")
    elif jogador == 2:
        print("EMPATE")
    else:
        print("JOGADA INVÁLIDA")