# VERIFICAR A MÉDIA DE UM ALUNO EM UMA DISCIPLINA

aluno = input("Qual é o seu nome: ")
disciplina = input("Disciplina: ")

qtd_notas = int(input("Digite a quantidade de notas: "))
notas = 0

for i in range(0, qtd_notas):
    notas += float(input("Digite a nota " + str(i + 1) + ": "))

media = float(notas/qtd_notas)
media = round(media,2) # formata a media em 2 casas decimais

print("A media do aluno",aluno, "na disciplina",disciplina, "foi: ",media)