# Exercicio 089 - Python - Curso em Video
# Boletim com notas compostas

ficha = list()

print()
print("-" * 30)
while True:
    nome = str(input("Nome: "))
    nota1 = float(input("Nota1: "))
    nota2 = float(input("Nota2: "))
    media = (nota1+nota2)/2
    ficha.append([nome, [nota1, nota2], media])
    resp = str(input("Deseja continuar? [S/N] "))
    if resp in 'Nn':
        break
    
print("-" * 30)
print(ficha)

print("-" * 30)
print(f'{"Id":<4}{"Nome":<10}{"Média":>8}')
print("-" * 30)

for i, a in enumerate(ficha):
    print(f'{i:<4}{a[0]:<10}{a[2]:>8.1f}')
    
while True:
    print("-" * 30)
    opc = int(input("Qual aluno deseja mostrar as notas? (999 interrompe)"))
    if opc == 999:
        break
    if opc <= len(ficha)-1:
        print(f' As Notas de {ficha[opc][0]} são {ficha[opc][1]}')