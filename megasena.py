# Exercicio 088 - Python - Curso em Video
# Palpites para Mega Sena
from random import randint
lista = list()

print()
print("-" * 30)
print("JOGO DA MEGA SENA")
print("-" * 30)

cont = 0
while True:
    num = randint(1,60)
    if num not in lista:
        lista.append(num)
        cont +=1
    if cont >=6:
        break

lista.sort()
print(f'Os numeros sorteados foram: {lista}')