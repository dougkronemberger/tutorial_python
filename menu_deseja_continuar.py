# Exercicio 070 - Python - Curso em Video
# Deseja Continuar?

print()
while True:
    produto = str(input("Nome do Produto: "))
    preco = float(input("Preço do Produto: R$ "))
    print()
    
    resp = ' '
    while resp not in 'SN':
        resp = str(input("Quer continuar? [S/N] ")).strip().upper()[0]
        print()
        
    if resp == 'N':
        break
        
print('{:-^40}'.format("FIM DO PROGRAMA"))