# Exercicio 029 - Python - Curso em Video
# Multa para carro que dirige acima de 80km/h pagando multa de R$ 7,00 por km acima do limite

print()
velocidade = int(input("Velocidade do carro: "))

if velocidade > 80:
    print("MULTADO. Voce ultrapassou a velocidade que é de 80Km/h")
    print("Tenha um bom dia")
    multa = (velocidade-80) * 7
    print("Voce deve pagar uma multa de R$ {:.2f}".format(multa))