# Exercicio 020 - Python - Curso em Video
from random import shuffle

print()
aluno1 = str(input("Aluno: "))
aluno2 = str(input("Aluno: "))
aluno3 = str(input("Aluno: "))
aluno4 = str(input("Aluno: "))
lista = [aluno1, aluno2, aluno3, aluno4]
shuffle(lista) # embaralha a ordem dos itens da lista

print()
print("-" *20)
print("A ordem de apresentação será: ")
print("-" *20)
print(lista)