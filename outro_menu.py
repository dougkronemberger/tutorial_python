# EXERCICIO 115 EM PYTHON - CURSO EM VIDEO
# MENU COMPLETO EM PYTHON COM FUNÇÕES : SOMENTE O MENU

print()
def leiaInt(msg):
    while True:
        try:
            n = int(input(msg))
        except (ValueError, TypeError):
            print('ERRO - DIGITE UM NÚMERO VÁLIDO')
            continue
        except (KeyboardInterrupt):
            print('USUÁRIO DECIDIU NÃO DIGITAR ESTE NÚMERO')
            return 0
        else:
            return n


def linha(tam = 40):
    return "-" * tam
    
def cabecalho(txt):
    print(linha())
    print(txt.center(40))
    print(linha())
    
def menu(lista):
    cabecalho("Menu Principal")
    c = 1
    for item in lista:
        print(f' {c} - {item}')
        c = c + 1
    print(linha())
    opc = leiaInt("Sua Opção: ")
    return opc
        
while True:
    resposta = menu(['Cadastrar Pessoa', 'Ver Pessoa Cadastrada', 'Sair do Sistema'])
    if resposta == 1:
        print("Opção 1")
    elif resposta == 2:
        print("Opção 2")
    elif resposta == 3:
        print("Saindo do sistema")
        break
    else:
        print("ERRO - DIGITE UMA OPÇÃO VÁLIDA")