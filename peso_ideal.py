# CALCULANDO PESO IDEAL COM BASE EM FÓRMULAS
# Para homens: (72.7 * altura) - 58
# Para mulheres: (62.1 * altura) - 44.7
# Peça o peso da pessoa e informe se ela está dentro, acima ou abaixo do peso.

nome = input("Informe seu nome: ")
sexo = input("Informe o seu sexo (M/F): ")
altura = float(input("Informe a sua altura (em metros): "))
peso = float(input("Informe o seu peso (em kg): "))
imc = (peso/altura**2)

if (sexo == "M"):
    imc = (72.7 * altura) - 58
    imc = round(imc,2)
else:
    imc = (62.1 * altura) - 44.7
    imc = round(imc,2)
if (peso > imc):
    print(nome, "Você está acima do seu peso ideal:", imc)
elif (peso < imc):
    print(nome, "Você está abaixo do seu peso ideal:", imc)
else:
    print(nome, "Você está no seu peso ideal:", imc)