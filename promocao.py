# Exercicio 012 - Python - Curso em Video
# Calcula desconto de 5% de um preço

print()
preco = float(input("Qual é o preço? R$"))
novo = preco - (preco * 5/100)

print("O preço custava R$ {:.2f} e na promocao de 5% custa R$ {:.2f}".format(preco,novo))
