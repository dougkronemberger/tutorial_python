# Exercicio 018 - Python - Curso em Video
import math

print()
angulo = float(input("Angulo desejado: "))
sen = math.sin(math.radians(angulo))
cos = math.cos(math.radians(angulo))
tan = math.tan(math.radians(angulo))

print()
print("-" *20)
print("Angulo: {:.0f} graus".format(angulo))
print("Seno: {:.2f}".format(sen))
print("Cosseno: {:.2f}".format(cos))
print("Tangente: {:.2f}".format(tan))