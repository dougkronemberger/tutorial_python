# Exercicio 003 - Python - Curso em Video
# Peça para o usuario digitar dois numeros e imprimir a soma entre eles

print()
n1 = int(input("Digite um numero: "))
n2 = int(input("Digite outro numero: "))
soma = n1+n2

print("A soma dos numeros {} e {} é: {}".format(n1,n2,soma))
