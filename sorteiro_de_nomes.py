# Exercicio 019 - Python - Curso em Video
import random

print()
aluno1 = str(input("Aluno: "))
aluno2 = str(input("Aluno: "))
aluno3 = str(input("Aluno: "))
aluno4 = str(input("Aluno: "))
lista = [aluno1, aluno2, aluno3, aluno4]
escolhido = random.choice(lista)

print()
print("-" *20)
print("O Aluno escolhido foi: {}".format(escolhido))

