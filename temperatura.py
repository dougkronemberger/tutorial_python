# Exercicio 014 - Python - Curso em Video

print()
c = float(input("Temperatura em Celsius: "))
f = ((9*c)/5)+32

print("-" * 40)
print("Temperatura em Celsius: {} ºC".format(c))
print("-" * 40)
print("Temperatura em Farenheit: {} ºF".format(f))
print("-" * 40)
