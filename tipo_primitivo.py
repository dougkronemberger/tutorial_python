# Exercicio 004 - Python - Curso em Video
# Dissecando uma variavel

print()
algo = input("Digite algo: ")

print("O tipo primitivo desse valor é: ", type(algo))
# verificando se só tem espaços
print("Só tem espaços: ", algo.isspace())
# verificando se é numero
print("Só tem numeros: ", algo.isnumeric())
# verificando se é alfabético (so tem alfabeto)
print("É alfabético: ", algo.isalpha())
# verificando se é alfanumérico
print("É alfanumérico:", algo.isalnum())
print("É maiuscula: ", algo.isupper())
print("É minuscula: ", algo.islower())