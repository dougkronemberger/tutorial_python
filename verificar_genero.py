# VERIFICAR SE UMA LETRA DIGITADA É F OU M: SE F = FEMININO; M = MASCULINO; OUTRO = INVALIDO

sexo = str(input('Digite (F)-Feminino, (M)-Masculino: ').upper())
if sexo == 'M':
    print('Sexo Masculino.')
elif sexo == 'F':
    print('Sexo Feminino.')
else:
    print('Sexo Inválido.')