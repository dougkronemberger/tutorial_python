# Votação python
print('''
Em uma votação presidencial, existem 3 candidatos.
Os votos são informados através de código. Os dados utilizados
para contagem obedecem à seguinte codificação:
1,2 e 3 = votos para os respectivos candidatos.
4 = voto nulo
5 = voto em branco
6 = fim dos votos computados

Elabore um programa que calcule e imprima:
- O total de votos para cada candidato.
- O total de votos nulos.
- O total de votos em branco.
- O percentual dos votos em branco e nulos sobre o total.
''')

print("-"*50)
v1 = 0
v2 = 0
v3 = 0
b = 0
n = 0
total = 0
op = 0
maior = 0
outros = 0

print("1 - Candidato 1: ")
print("2 - Candidato 2: ")
print("3 - Candidato 3: ")
print("4 - Votar Nulo: ")
print("5 - Votar em Branco: ")
print("0 - Sair: ")

while True:
    op = int(input("Qual sua opção de voto: (Digite entre 1 a 5 ou 0 para sair): "))

    if op == 1:
        v1+=1
        total+=1
    elif op == 2:
        v2+=1
        total+=1
    elif op == 3:
        v3 +=1
        total+=1
    elif op == 4:
        n+=1
        total+=1
    elif op == 5:
        b+=1
        total+=1
    elif op == 0:
        break
        
    resp = ' '
    while resp not in 'SN':
        resp = str(input("Quer continuar? [S/N] ")).strip().upper()[0]
        print()
        
    if resp == 'N':
        break

if (op > 0) and (op <= 5):
    votos_b = (b*100)/total
    votos_n = (n*100)/total
    print("Votos Nulos: {} voto(s) com {:.2f} %".format(n,votos_n))
    print("Votos em Branco: {} voto(s) com {:.2f} %".format(b,votos_b))

print("-"*50)
print("Candidato 1: {} voto(s)".format(v1))
print("Candidato 2: {} voto(s)".format(v2))
print("Candidato 3: {} voto(s)".format(v3))